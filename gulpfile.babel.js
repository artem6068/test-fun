import gulp from 'gulp';

import browserSync from 'browser-sync';

require('require-dir')('tasks');

gulp.task('serve', () => {
  browserSync.init({
    server: "./app"
  });
});

gulp.task('default', gulp.series(
  gulp.parallel('pug:json', 'copy'),
  gulp.parallel('pug', 'sass', 'js'),
  gulp.parallel('watch', 'serve')
));

gulp.task('deploy', gulp.series(
  gulp.parallel('pug:json', 'copy'),
  gulp.parallel('pug', 'sass:deploy', 'js:deploy'),
  gulp.parallel('watch', 'serve')
));
