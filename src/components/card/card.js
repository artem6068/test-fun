export const Card = () => {
  const card = document.querySelectorAll('.js-card');
  const cardContents = document.querySelectorAll('.js-card__content');

  let cardContentHeight = 0;

  for (let element of card) {
    const cardActive = element.querySelectorAll('.js-card__active');
    const thisContentHeight = element.querySelector('.js-card__content').offsetHeight;

    if (thisContentHeight > cardContentHeight) {
      cardContentHeight = thisContentHeight;
    }

    if (!element.classList.contains('disable')) {
      for (let el of cardActive) {

        el.onmouseenter = () => {
          if (!element.classList.contains('active')) {
            element.classList.add('hover');
          }
        }
        el.onmouseleave = () => {
          if (element.classList.contains('active')) {
            element.classList.add('hover');
          } else {
            element.classList.remove('hover');
          }
        }

        el.onclick = () => {
          if (!element.classList.contains('active')) {
            element.classList.add('active');
            element.classList.remove('hover');
          } else {
            element.classList.remove('active');
          }
        }
      }
    }
  }

  for (let element of cardContents) {
    element.style.height = `${cardContentHeight}px`;
  }

};
