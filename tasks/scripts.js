import gulp from 'gulp';

import browserify from 'browserify';
import babelify from 'babelify';
import source from 'vinyl-source-stream';
import buffer from 'vinyl-buffer';
import uglify from 'gulp-uglify';

import browserSync from 'browser-sync';
import sourcemaps from 'gulp-sourcemaps';
import notify from 'gulp-notify';

gulp.task('js', () => {
  return browserify('./src/static/js/main.js', {debug: true})
    .transform(babelify)
    .bundle()
    .on("error", notify.onError({
      message: "<%= error.message %>",
      title: "Error js"
    }))
    .pipe(source('main.js'))
    .pipe(buffer())
    .pipe(sourcemaps.init({loadMaps: true}))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('./app/static/js/'))
    .on('end', browserSync.reload);
});

gulp.task('js:deploy', () => {
  return browserify('./src/static/js/main.js')
    .transform(babelify)
    .bundle()
    .on("error", notify.onError({
      message: "<%= error.message %>",
      title: "Error js"
    }))
    .pipe(source('main.js'))
    .pipe(buffer())
    .pipe(uglify())
    .pipe(gulp.dest('./app/static/js/'));
});
