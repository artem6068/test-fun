import gulp from 'gulp';

import sass from 'gulp-sass';
import autoprefixer from 'gulp-autoprefixer';
import csso from 'gulp-csso';

import browserSync from 'browser-sync';
import sourcemaps from 'gulp-sourcemaps';
import notify from 'gulp-notify';

gulp.task('sass', () => {
  return gulp.src('./src/static/sass/main.scss')
    .pipe(sourcemaps.init())
      .pipe(sass())
      .on("error", notify.onError({
        message: "<%= error.message %>",
        title: "Error sass"
      }))
      .pipe(autoprefixer({
        browsers: ['last 4 versions']
      }))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('./app/static/css/'))
    .pipe(browserSync.reload({
      stream: true
    }));
});

gulp.task('sass:deploy', () => {
  return gulp.src('./src/static/sass/main.scss')
    .pipe(sass())
    .on("error", notify.onError({
      message: "<%= error.message %>",
      title: "Error sass"
    }))
    .pipe(autoprefixer({
      browsers: ['last 4 versions']
    }))
    .pipe(csso({}))
    .pipe(gulp.dest('./app/static/css/'));
});
